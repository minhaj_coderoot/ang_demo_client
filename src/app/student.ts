export class Student {
    id: number;
    first_name: string;
    last_name: string;
    address: string;
    updated_at: Date;
  }