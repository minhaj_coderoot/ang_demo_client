import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap,  } from 'rxjs/operators';
import { Student } from './student';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const apiUrl = "http://192.168.43.226/cruds";
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) { }

  addStudent(student) {
    return this.http.post(apiUrl+ '/Students_con/add_student', student, httpOptions).pipe(
      tap((product: Student) => 
      console.log(`added product w/ id=${product.id}`)),
      catchError(
        this.handleError<Student>('addProduct'))
    );
  }
  getStudent() {
    return this.http.get(apiUrl+ '/Students_con/get_students')
    .pipe(
      tap(heroes => 
        console.log('fetched products')),
      catchError(
        this.handleError('getProducts', []))
    );
    // return new Promise(resolve => {
    // //   this.http.get(apiUrl+ '/Students_con/get_students').map(res => res.json())
    // //     .subscribe(
    // //       res => {
    // //         if (res) {
    // //           resolve(res);
    // //         } else {
    // //           resolve(false);
    // //         }
    // //       }, error => {
    // //         resolve(false);
    // //       }
    // //     );
    // // });
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
